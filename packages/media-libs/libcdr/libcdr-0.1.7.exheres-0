# Copyright 2013 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Library providing ability to interpret and import Corel Draw drawings"
DESCRIPTION="
libcdr currently supports just CDR files from V7 to X3 and the following features:
- pages and page sizes
- shapes, lines, Bezier curves, bitmaps
- flat color fills, both RGB, CMYK, HLS and HSB
- different kinds of strokes including dashed strokes
"
HOMEPAGE="https://wiki.documentfoundation.org/DLP/Libraries/${PN}"
DOWNLOADS="https://dev-www.libreoffice.org/src/${PN}/${PNV}.tar.xz"

LICENCES="MPL-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-libs/boost[>=1.41.0]
        virtual/pkg-config[>=0.20]
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/icu:=
        media-libs/lcms2
        office-libs/librevenge[>=0.0.1]
        sys-libs/zlib
    test:
        dev-cpp/cppunit
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-tools
    --disable-fuzzers
    --disable-werror
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'doc docs' )

DEFAULT_SRC_CONFIGURE_TESTS=( '--enable-tests --disable-tests' )

